
# Ejercicio 1

Creamos el repositorio en Gitlab

![Imagen 2](ejercicio2.png "titulo")


Iniciamos repositorio

![Imagen 1](ejercicio1.png "titulo")


# Ejercicio 2

Creamos unidad organizativa y alumnos, lo añadimos a LDAP

![Imagen 3](ejercicio3.png "titulo")
![Imagen 4](ejercicio4.png "titulo")

Creamos proyecto Eclipse

![Imagen 5](ejercicio5.png "titulo")

Context.xml 

![Imagen 6](ejercicio6.png "titulo")

web.xml

![Imagen 7](ejercicio7.png "titulo")

## Ejercicio 3


Configuramos el archivo hosts de tomcat

![Imagen 8](ejercicio8.png "titulo")

Habilitamos el host de tomcat

![Imagen 9](ejercicio9.png "titulo")

Reiniciamos tomcat

![Imagen 10](ejercicio10.png "titulo")

Creamos el keytool

![Imagen 11](ejercicio11.png "titulo")

Modificamos el archivo server.xml para configurar el connector 

![Imagen 12](ejercicio12.png "titulo")




